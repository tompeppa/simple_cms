from django.db import models


# Create your models here.
class Keyword(models.Model):
    word = models.CharField(verbose_name='关键词', max_length=128, unique=True)
    create_date = models.DateTimeField(verbose_name='创建时间', auto_now_add=True)

    enable = models.BooleanField(default=True, verbose_name='是否启用')

    def __str__(self):
        return self.word

    class Meta:
        verbose_name = '词库'
        verbose_name_plural = '词库管理'


class History(models.Model):
    word = models.CharField(verbose_name='关键词', max_length=128)
    create_date = models.DateTimeField(verbose_name='创建时间', auto_now_add=True)

    def __str__(self):
        return self.word

    class Meta:
        verbose_name = '历史'
        verbose_name_plural = '采集历史'


class Sensitive(models.Model):
    word = models.CharField(verbose_name='关键词', max_length=32)
    replace = models.CharField(verbose_name='替换词', max_length=32, default='')

    def __str__(self):
        return self.word

    class Meta:
        verbose_name = '敏感词'
        verbose_name_plural = '敏感词库'
