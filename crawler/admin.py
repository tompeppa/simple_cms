from django.contrib import admin
from import_export import resources
from import_export.admin import ImportExportModelAdmin, ImportExportActionModelAdmin
from crawler.models import *


# Register your models here.

@admin.register(Keyword)
class KeywordAdmin(admin.ModelAdmin):
    list_per_page = 20
    search_fields = ('word',)

    list_display = ('id', 'word', 'enable', 'create_date')


@admin.register(History)
class HistoryAdmin(admin.ModelAdmin):
    list_per_page = 20
    search_fields = ('word',)

    list_display = ('id', 'word', 'create_date')


class ProxyResource(resources.ModelResource):
    class Meta:
        model = Sensitive
        import_id_fields = ('word',)
        fields = ('word', 'replace')


@admin.register(Sensitive)
class SensitiveAdmin(ImportExportModelAdmin):
    resource_class = ProxyResource
    list_per_page = 50
    search_fields = ('word', 'replace')
    list_display = ('id', 'word', 'replace')
    list_editable = ('word', 'replace')
