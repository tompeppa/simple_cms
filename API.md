# api接口文档

+ 系统级参数

系统级参数均由header中传入

|字段|类型|说明|
|-----|-----|-----|
|token|uuid string|授权令牌uuid字符串，后台接口管理>授权管理中添加|

+ 返回参数

|字段|类型|说明|
|-----|-----|-----|
|status|int|0=失败，1=成功|
|msg|string|提示字符串|

+ 发布文章接口

地址：
> /api/post

|字段|类型|说明|必填|
|----|----|----|----|
|title|string|标题|必填|
|content|string|内容|必填|
|summary|string|简介|必填|
|type|int|文章类型，0=html，1=markdown|选填|
|published|boolean|是否发布，true=发布，false=不发布，默认为false|选填|
|cover|string|封面，如果包含http或者https将默认下载到本地|选填|






