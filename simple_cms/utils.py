from django.http import HttpResponse
from django.template import loader
import re


def compression(html):
    """
    移除多余的空格，换行
    :param html:
    :return:
    """
    html = re.sub("\r", "", html)
    html = re.sub("\n|\t", " ", html)
    html = re.sub("[ ]{2,}", " ", html)

    return html


def render(request, template_name, context=None, content_type=None, status=None, using=None):
    """
    移除多余的空格，换行符等
    :param request:
    :param template_name:
    :param context:
    :param content_type:
    :param status:
    :param using:
    :return:
    """
    content = loader.render_to_string(template_name, context, request, using=using)
    # 压缩
    content = compression(content)

    return HttpResponse(content, content_type, status)
