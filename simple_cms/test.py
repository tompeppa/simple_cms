import os
import sys

import django
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "simple_cms.settings")
django.setup()

from cms.models import Article

rs = Article.objects.all()
for a in rs:
    title = a.title

    if title.find('防治') != -1 or title.find('治疗') != -1 or title.find('症状') != -1:
        a.category_id = 3
    elif title.find('百科') != -1:
        a.category_id = 1
    else:
        a.category_id = 2

    a.save()
