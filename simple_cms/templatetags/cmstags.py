import datetime
import math
import random

from django import template
from django.core.paginator import Paginator
from django.template.loader import get_template

from ad.models import Ad
from system.models import *
from cms.models import *
import re
import json

register = template.Library()  # 这一句必须这样写


@register.simple_tag
def load_settings():
    # TODO 可以加缓存
    results = {}
    fields = ('key', 'value', 'label', 'type')
    data = list(SystemConfig.objects.values(*fields))
    for item in data:
        results[item.get('key')] = item.get('value')

    return results


@register.simple_tag
def load_links():
    return Links.objects.order_by('sort').values('url', 'name')


@register.simple_tag
def load_navbar(type=0):
    return Navbar.objects.filter(type__in=(type, 2)).values('name', 'url').order_by('sort')


@register.simple_tag
def get_year():
    return datetime.datetime.now().year


@register.filter
def filter_img(val):
    return val or '/static/image/no_image.png'


@register.simple_tag
def get_new_article(size=10):
    return Article.objects.filter(published=True).order_by('-id').values('id', 'title', 'create_date',
                                                                         'category__alias', 'category__name',
                                                                         'cover', 'hits', 'summary')[:size]


@register.simple_tag
def get_random_article(size=10):
    count = Article.objects.filter(published=True).count()
    if count < size:
        return []
    ids = []
    index = 0

    # 数据库随机查询性能过低
    while index < size:
        if count - 1 == 0:
            break

        id = random.randint(0, count - 1)
        if not id in ids:
            ids.append(id)
            index += 1

    articles = []
    for id in ids:
        r = Article.objects.filter(published=True).values('id', 'title', 'create_date',
                                                          'category__alias', 'category__name',
                                                          'cover', 'hits', 'summary')[id]
        articles.append(r)

    return articles


@register.simple_tag
def get_hot_article(size=10):
    return Article.objects.filter(published=True).order_by('-hits').values('id', 'title', 'create_date',
                                                                           'category__alias', 'category__name',
                                                                           'cover', 'hits', 'summary')[:size]


@register.filter
def get_opacity(val):
    return (11 - val) / 10


@register.simple_tag
def get_prev(id):
    """获取前一篇文章"""
    return Article.objects.filter(id__lt=id, published=True).order_by('-id').values('id', 'title',
                                                                                    'category__alias').first()


@register.simple_tag
def get_next(id):
    """获取前一篇文章"""
    return Article.objects.filter(id__gt=id, published=True).order_by('id').values('id', 'title',
                                                                                   'category__alias').first()


@register.simple_tag
def get_category(alias, page=1, size=10):
    params = {
        'published': True
    }
    if alias:
        params['category__alias'] = alias

    articles = Article.objects.filter(**params).order_by('-id').values('id', 'title', 'create_date',
                                                                       'category__alias', 'category__name',
                                                                       'cover', 'hits', 'summary')
    paginator = Paginator(articles, size)
    url = '/{}/p/'.format(alias)
    if not alias:
        url = '/topic/'
    return {
        'count': paginator.count,
        'num_pages': paginator.num_pages,
        'list': paginator.page(page),
        'num_size': size,
        'current_page': page,
        'url': url
    }


@register.simple_tag
def get_tag(name, page=1, size=10):
    values = ('id', 'title',
              'create_date',
              'category__alias',
              'category__name',
              'cover', 'hits',
              'summary')

    articles = Article.objects.filter(published=True, tags__name=name).order_by('-id').values(*values)
    paginator = Paginator(articles, size)
    url = '/tag/{}/p/'.format(name)

    return {
        'count': paginator.count,
        'num_pages': paginator.num_pages,
        'list': paginator.page(page),
        'num_size': size,
        'current_page': page,
        'url': url
    }


@register.simple_tag
def get_tag_random(size=5):
    count = Tag.objects.count()
    if count < size:
        return []
    ids = []
    index = 0

    # 数据库随机查询性能过低
    while index < size:
        if count - 1 == 0:
            break

        id = random.randint(0, count - 1)
        if not id in ids:
            ids.append(id)
            index += 1

    tags = []
    for id in ids:
        r = Tag.objects.values('name')[id]
        tags.append(r)

    return tags


def get_page_range(page_num, show_num, current_page):
    # 分偶数和奇数
    current_page = int(current_page)
    page_num = int(page_num)
    show_num = int(show_num)

    if show_num % 2 == 0:
        fore = int((show_num - 1) / 2)
        after = int(show_num / 2)
    else:
        fore = int(show_num / 2)
        after = math.ceil((show_num - 1) / 2)

    star = current_page - fore
    end = current_page + after
    if star == 0:
        star = 1
        end += star
    elif star < 0:
        end += int(math.fabs(star))
        end += 1
        star = 1

    if end > page_num:
        star -= end - page_num
        end = page_num

    if star < 1:
        star = 1
    if end > page_num:
        end = page_num
    return star, end


@register.simple_tag
def get_mobile_paginator(num_page_count, current_page, show_num, url):
    template = 'mobile/paginator.html'
    return get_paginator(num_page_count, current_page, show_num, url, template)


@register.simple_tag
def get_paginator(num_page_count, current_page, show_num, url, template='paginator.html'):
    num_page_count = int(num_page_count)
    current_page = int(current_page)
    show_num = int(show_num)

    start, end = get_page_range(num_page_count, show_num, current_page)

    prev = current_page - 1
    next = current_page + 1

    page_list = []
    for i in range(start, end + 1):
        page_list.append(i)

    engine = get_template(template)
    html = engine.render({
        'url': url,
        'num_pages': num_page_count,
        'current_page': current_page,
        'show_num': show_num,
        'start': start,
        'end': end,
        'prev': prev,
        'next': next,
        'page_list': page_list
    })
    return format_html(html)


@register.simple_tag
def get_all_category():
    return Category.objects.order_by('sort').values('name', 'alias')


@register.simple_tag
def get_article(category_id=None, size=10):
    params = {
        'published': True
    }
    if category_id:
        params['category_id'] = category_id
    return Article.objects.filter(**params).order_by('-id').values('id', 'title', 'create_date',
                                                                   'category__alias', 'category__name',
                                                                   'cover', 'hits', 'summary')[:size]


@register.filter
def test(val):
    print(val)


@register.simple_tag
def now_utctime():
    return sitemap_date(datetime.datetime.now())


@register.filter
def sitemap_date(val):
    return (val - datetime.timedelta(hours=8)).strftime('%Y-%m-%dT%H:%M:%S+08:00')


@register.simple_tag
def get_banner(size=6):
    return Article.objects.filter(top=True, published=True).order_by('-update_date').values('id', 'category__alias',
                                                                                            'title',
                                                                                            'cover')[:size]


@register.simple_tag
def get_ad(key, platform=0, style=''):
    ad = Ad.objects.filter(key=key, platform=platform, published=True).first()
    engine = get_template('ad.html')
    html = engine.render({
        'ad': ad,
        'style': style
    })

    return format_html(html)


@register.simple_tag(takes_context=True)
def get_current_host(context):
    req = context.request
    return "{}://{}".format(req.scheme, req.META.get("HTTP_HOST"))


@register.simple_tag(takes_context=True)
def get_current_url(context):
    req = context.request
    return get_current_host(context) + req.path
