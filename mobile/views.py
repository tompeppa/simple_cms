# from django.shortcuts import render
from django import shortcuts

from cms.models import Page, Category, Article, Tag

from simple_cms import utils

from django.views.decorators.cache import cache_page


def render(request, template_name, context=None, content_type=None, status=None, using=None):
    # return shortcuts.render(request, 'mobile/{}'.format(template_name), context, content_type, status, using)
    return utils.render(request, 'mobile/{}'.format(template_name), context, content_type, status, using)


@cache_page(60 * 10, key_prefix='mobile')
def index(request):
    return render(request, 'index.html')


@cache_page(60 * 10, key_prefix='mobile')
def category(request, alias, page=1):
    # 先找自定义页面，如果没有才是分类

    p = Page.objects.filter(type__in=(1, 2), alias=alias).first()
    if p:
        template = 'page.html'

        return render(request, template, {
            'page': p
        })
    else:
        return render(request, 'category.html', {
            'alias': alias,
            'current_page': page,
            'category': Category.objects.filter(alias=alias).first()
        })


# 文章缓存24小时
@cache_page(60 * 60 * 24, key_prefix='mobile')
def aritlce(req, alias, id):
    a = Article.objects.get(id=id)
    a.hits += 1
    a.save()

    return render(req, 'article.html', {
        'article': a,
        'alias': alias
    })


@cache_page(60 * 10, key_prefix='mobile')
def tag(request, name, page=1):
    tag = Tag.objects.filter(name=name).first()
    return render(request, 'tag.html', {
        'page': page,
        'tag': tag
    })
