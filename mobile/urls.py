from django.urls import path
from django.views.generic import RedirectView

from mobile import views
from simple_cms import views as pc_views

urlpatterns = [
    path('', views.index, name='index'),
    path('sitemap.xml', pc_views.sitemap, name='sitemap'),
    path('sitemap.xsl', pc_views.sitemap_xsl, name='sitemap_xsl'),
    path('tag/<name>.html', views.tag, name='tag'),
    path('tag/<name>/p/<page>', views.tag, name='tag_page'),
    path('<alias>/', views.category, name='category'),
    path('<alias>/p/<page>', views.category, name='category_page'),
    path('favicon.ico', RedirectView.as_view(url='/static/image/favicon.ico')),
    path('<alias>/<id>.html', views.aritlce, name='article'),
]
