from django.contrib import admin
from api.models import *


# Register your models here.

@admin.register(Token)
class TokenAdmin(admin.ModelAdmin):
    search_fields = ('token',)
    list_display = ('id', 'token', 'description', 'create_date', 'update_date')
    list_per_page = 20
