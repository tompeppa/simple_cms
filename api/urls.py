from django.contrib import admin
from django.urls import path, include
from django.views.generic import RedirectView

from api import views

urlpatterns = [
    path('post/', views.post)
]
