import uuid

from django.db import models


# Create your models here.

class Token(models.Model):
    token = models.UUIDField(default=uuid.uuid4, verbose_name='令牌', null=True, blank=True, help_text='访问接口的时候校验该值',
                             db_index=True)
    description = models.CharField(max_length=128, verbose_name='描述', null=True, blank=True)

    create_date = models.DateTimeField(auto_now_add=True, verbose_name='创建时间', null=True, blank=True)
    update_date = models.DateTimeField(auto_now=True, verbose_name='更新时间', null=True, blank=True)

    def __str__(self):
        return str(self.token)

    class Meta:
        verbose_name = '授权'
        verbose_name_plural = '授权管理'
