# 广告说明

在cmstags中有个get_ad标签 可以加载指定key的广告

例如：

```
{% get_ad 'key'}

# 加载手机
{% get_ad 'key' 1}

# 加载PC
{% get_ad 'key' 0}

# 加载自定义css
{% get_ad 'key' 1 'color:red;width:1000px'}

```

广告需要自己添加到页面中