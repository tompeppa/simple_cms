# simple cms

手机版URL：
> localhost:8000/mobile

PC版URL：
> localhost:8000

## 初始化项目

```
git clone git@github.com:newpanjing/simple_cms.git
```

## 安装依赖包

```
pip install -r requirements.txt
```

## 配置数据库
找到项目中的settings.py文件，替换数据库的host和用户名密码，在执行以下步骤

## 执行迁移

### 生成迁移文件
```
python manage.py makemigrations
```

### 执行迁移
```
python manage.py migrate
```

## 创建超级用户

```
python manage.py createsuperuser
```

## 启动查看

```
python manage.py runserver 8000
```

## 访问地址

### 前台地址

```
http://localhost:8000
```

### 后台地址

```
http://localhost:8000/admin
```

用户名密码创建步骤请看：[创建超级用户](创建超级用户)

# API接口
[API接口地址](./API.md)

# 广告位说明
[AD](./AD.md)

# 数据库配置文件
`simple_cms/config.py`

# 手机版和PC版配置

nginx 配置文件：

```nginx

   server {
      listen 80;
        server_name 56bird.cn;
        root html;
        rewrite ^(.*)$  http://www.56bird.cn$1 permanent;
    }

   server {
        listen 80;
        server_name www.56bird.cn m.56bird.cn;
        charset utf-8;
        location /favicon.ico {
            expires 30d;
            root /data/www/56bird_cn/static/;
        }
        location /static {
           expires 30d;
           root /data/www/56bird_cn/;
        }
        location / {
                proxy_pass http://127.0.0.1:8001;
                 proxy_read_timeout 300;
                        proxy_set_header   Host             $host;
                        proxy_set_header   X-Real-IP        $remote_addr;
                        proxy_set_header   X-Forwarded-For  $proxy_add_x_forwarded_for;
    
        }
   }
```

# 启动
进入根目录

1. 创建虚拟环境
```
virtualenv venv    
```
如果没有virtualenv 命令请先安装
pip install virtualenv 或用apt和yum：apt-get install virtualenv，yum install virtualenv -y  
2. 进入虚拟环境

```
source venv/bin/activate
```
3. 安装依赖包
```
pip install -r requirements.txt
```

4. 运行根目录start.sh
```shell
sh start.sh
```

# 域名
+ PC域名
www.xxx.com

+ 手机域名
m.xxx.com

手机或者PC的域名在simple_cms/hosts.py文件中修改，随后nginx的配置文件也要修改


# 注意事项

本程序开放源码，但是不提供任何技术支持，有问题请自行百度。