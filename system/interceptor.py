from django.http import HttpResponse
from django.utils.deprecation import MiddlewareMixin

from api.models import Token

import json

from system.models import SpiderLog


class SpiderMiddleware(MiddlewareMixin):

    def process_request(self, request):
        # 校验token
        if request.path.find('/sitemap.xml') == 0:
            try:
                ua = request.META.get('HTTP_USER_AGENT')
                if 'HTTP_X_FORWARDED_FOR' in request.META:
                    ip = request.META['HTTP_X_FORWARDED_FOR']
                else:
                    ip = request.META['REMOTE_ADDR']
                url = '{}{}'.format(request.scheme + "://" + request.META.get("HTTP_HOST"), request.path)
                SpiderLog.objects.create(
                    ua=ua,
                    ip=ip,
                    url=url
                )
                print('蜘蛛日志入库：{}={}={}'.format(ua, ip, url))
            except Exception as e:
                print('蜘蛛监控发生错误')
                print(e)
