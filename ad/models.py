from django.db import models


# Create your models here.

class Ad(models.Model):
    name = models.CharField(verbose_name='广告位名', max_length=128)
    key = models.CharField(verbose_name='key', max_length=128, help_text='页面上的标签根据此值获取', db_index=True)
    value = models.TextField(verbose_name='值', default='', null=True, blank=False,
                             help_text='如果类型是图片就填图片地址，是链接就填显示的文本值，html代码就直接填')
    type = models.IntegerField(choices=(
        (0, '图片'),
        (1, '链接'),
        (2, 'html')
    ), verbose_name='类型', default=0)

    target = models.IntegerField(choices=(
        (0, '新标签'),
        (1, '当前页')
    ), verbose_name='跳转类型', default=0, help_text='类型为html的时候 该值无效', null=True, blank=True)

    url = models.CharField(verbose_name='链接地址', default='javascript:;', max_length=128, null=True, blank=True,
                           help_text='类型为html的时候 该值无效')

    create_date = models.DateTimeField(auto_now_add=True, verbose_name='创建时间', null=True, blank=True)
    update_date = models.DateTimeField(auto_now=True, verbose_name='更新时间', null=True, blank=True)

    published = models.BooleanField(verbose_name='是否发布', default=True, db_index=True)

    platform = models.IntegerField(choices=(
        (0, '电脑'),
        (1, '手机')
    ), default=0, verbose_name='平台')

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = '广告'
        verbose_name_plural = '广告管理'
