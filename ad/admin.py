from django.contrib import admin

from ad.models import *


# Register your models here.
@admin.register(Ad)
class AdAdmin(admin.ModelAdmin):
    list_display = (
    'id', 'name', 'key', 'value', 'type', 'target', 'url', 'create_date', 'update_date', 'published', 'platform')
    list_per_page = 10
    list_filter = ('type', 'target', 'platform')
    search_fields = ('name', 'key')
